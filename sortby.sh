#!/bin/bash
cd /home/"$USER"/Téléchargements || exit
for zik in *.wav *.mp3 *.ogg *.flac; do
  [[ -e "$zik" ]] || break # handle the case of no *.wav *.mp3 *.ogg *.flac files
  echo "$zik"
  mv "$zik" "/home/""$USER""/Musique/"
done
for doc in *.pdf *.doc *.odt *.txt; do
  [[ -e "$doc" ]] || break # handle the case of no *.pdf *.doc *.odt *.txt files
  echo "$doc"
  mv "$doc" "/home/""$USER""/Documents/pdf/"
done
for img in *.jpg *.png *.jpeg; do
  [[ -e "$img" ]] || break # handle the case of no *.jpg *.png *.jpeg files
  echo "$img"
  mv "$img" "/home/""$USER""/Images/"
done
for vid in *.mp4 *.avi *.mkv *.webm; do
  [[ -e "$vid" ]] || break # handle the case of no *.mp4 *.avi *.mkv *.webm files
  echo "$vid"
  mv "$vid" "/home/""$USER""/Vidéos/"
done
